<?php

interface SomeInterface
{
    public function handle(): string;
}

class SomeObjectOne implements SomeInterface
{
    public function handle(): string
    {
        return 'handle_object_1';
    }
}

class SomeObjectTwo implements SomeInterface
{
    public function handle(): string
    {
        return 'handle_object_2';
    }
}

class SomeObjectsHandler
{
    public function __construct()
    {
    }

    public function handleObjects(array $objects): array
    {
        $handlers = [];
        foreach ($objects as $object) {
            $handlers[] = $object->handle();
        }

        return $handlers;
    }
}

$objects = [
    new SomeObjectOne(),
    new SomeObjectTwo()
];

$soh = new SomeObjectsHandler();
$soh->handleObjects($objects);
