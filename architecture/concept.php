<?php

class Config
{
    public function getConfigParamByName(string $nameConfig): string
    {
        return '';
    }
}

interface StoreKeyInterface
{
    public function getSecretKey(): string;

    public function getDriverName(): string;
}

class FileStoreKey implements StoreKeyInterface
{
    private string $driverName = 'file';

    public function getSecretKey(): string
    {
        return '';
    }

    public function getDriverName(): string
    {
        return $this->driverName;
    }
}

class DBStoreKey implements StoreKeyInterface
{
    private string $driverName = 'db';

    public function getSecretKey(): string
    {
        return '';
    }

    public function getDriverName(): string
    {
        return $this->driverName;
    }
}

class StoreManager
{
    private string $nameDriverFromConfig;

    public function __construct(Config $config)
    {
        $this->nameDriverFromConfig = $config->getConfigParamByName('store_name');
    }

    /**
     * @throws ReflectionException
     */
    public function getSecretKey()
    {
        $classes = get_declared_classes();
        foreach($classes as $class) {
            $reflect = new ReflectionClass($class);
            if ($reflect->implementsInterface('StoreKeyInterface')) {
                $instance = $reflect->newInstance();
                if ($instance->getDriverName() === $this->nameDriverFromConfig) {
                    return $instance->getSecretKey();
                }
            }
        }

        return '';
    }
}


class Concept
{
    private Client $client;
    private StoreManager $storeManager;

    public function __construct(StoreManager $storeManager, Client $httpClient)
    {
        $this->storeManager = $storeManager;
        $this->client = $httpClient;
    }

    public function getUserData()
    {
        $params = [
            'auth' => ['user', 'pass'],
            'token' => $this->storeManager->getSecretKey()
        ];

        $request = new \Request('GET', 'https://api.method', $params);
        $promise = $this->client->sendAsync($request)->then(function ($response) {
            $result = $response->getBody();
        });

        $promise->wait();
    }
}
