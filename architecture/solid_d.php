<?php

interface XMLHTTPRequestInterface
{
    public function request(string $url, string $method, array $options = []);
}

class XMLHTTPRequestService implements XMLHTTPRequestInterface
{

    public function request(string $url, string $method, array $options = [])
    {
        // TODO: Implement request() method.
    }
}

class XMLHttpService extends XMLHTTPRequestService
{
}

class Http
{
    private $service;

    public function __construct(XMLHTTPRequestInterface $xmlHttpService)
    {
        $this->service = $xmlHttpService;
    }

    public function get(string $url, array $options)
    {
        $this->service->request($url, 'GET', $options);
    }

    public function post(string $url)
    {
        $this->service->request($url, 'GET');
    }
}

$service = new XMLHttpService();
$http= new Http($service);
