<?php
$array = [
    ['id' => 1, 'date' => '12.01.2020', 'name' => 'test1'],
    ['id' => 2, 'date' => '02.05.2020', 'name' => 'test2'],
    ['id' => 4, 'date' => '08.03.2020', 'name' => 'test4'],
    ['id' => 1, 'date' => '22.01.2020', 'name' => 'test1'],
    ['id' => 2, 'date' => '11.11.2020', 'name' => 'test4'],
    ['id' => 3, 'date' => '06.06.2020', 'name' => 'test3']
];

/**
 * Отсортировать многомерный массив по ключу(любому)
 */
usort($array, function ($current, $next) {
    return $current['id'] <=> $next['id'];
});

/**
 * Выделить уникальные записи(убрать дубли) в отдельный массив
 * В конечном массиве не должно быть элементов с одинаковым id
 */
$prevValue = -1;
$uniqueArray = array_filter($array, function ($item) use (&$prevValue) {
    if ($item['id'] !== $prevValue) {
        $prevValue = $item['id'];
        return true;
    }

    return false;
});

/**
 * Вернуть из массива только элементы, удовлетворяющие внешним
 * условиям (например элементы с определенным id). Пусть будет 1, 2 и 3
 */
$conditionArray = array_filter($array, function ($item) {
    return in_array($item['id'], [1, 2, 3]);
});


/**
 * Изменить в массиве значения и ключи (использовать name => id в качестве
 * пары ключ => значение)
 */
$result = array_combine(
    array_column($array, 'name'),
    array_column($array, 'id')
);
