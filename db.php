<?php
/**
 * table 'goods':
 * - id INTEGER
 * - name TEXT
 *
 * table 'tags':
 * - id INTEGER
 * - name TEXT
 *
 * plot table 'goods_tags':
 * - tag_id INTEGER
 * - goods_id INTEGER
 * - UNIQUE(tag_id, goods_id)
 *
 * Выведите id и названия всех товаров, которые
 * имеют все возможные теги в этой базе
 */

$sql = `SELECT g.id, g.name
        FROM goods g JOIN goods_tags gt
            ON gt.goods_id = g.id
        GROUP BY g.id, g.name
        HAVING COUNT(DISTINCT gt.tag_id) = (SELECT COUNT(t.id) FROM tags t)`;

/**
 * table 'evaluations':
 * - respondent_id UUID primary key -- ID респондента
 * - department_id UUID -- ID департамента
 * - gender BOOLEAN -- true — мужчина, false — женщина
 * - value INTEGER -- Оценка
 *
 * Выбрать без join-ов и подзапросов все департаменты,
 * в которых есть мужчины, и все они (каждый) поставили\
 * высокую оценку (строго выше 5)
 */

$sql = `SELECT department_id
        FROM evaluations
        WHERE gender=true AND value > 5
        GROUP BY department_id
        HAVING MIN(value) > 5`;
